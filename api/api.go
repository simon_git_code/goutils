package api
import(
	"net/http"
	"encoding/json"
	"net/url"
	"regexp"
	"strings"
	"runtime/debug"
	"log"
	"context"
	"fmt"
	"time"
	"gitee.com/simon_git_code/goutils/util"
	"gitee.com/simon_git_code/goutils/server"
	"io/ioutil"
)
type Api struct{
	Service server.Service
} 
type responseResult struct {
	Msg interface{} `json:"msg"`
	Data map[string]interface{} `json:"data"`
	// Data map[string]string `json:"data"`
	Code int64 `json:"code"`
	Success bool `json:"success"`
}

type ProxRequest struct {
	R map[string]interface{}
	Service string
	Method string
}
type Reply struct{
	Result map[string]interface{}
    // Detail map[string]string
} 
func(api *Api) Run () {
	mux := http.NewServeMux()
	mux.HandleFunc("/", api.MainHandler)
	err := http.ListenAndServe("0.0.0.0:9092", mux)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
func (api *Api) Health (addr *string,fun func(w http.ResponseWriter, r *http.Request)) {
	mux := http.NewServeMux()
	mux.HandleFunc("/", fun)
	err := http.ListenAndServe(*addr, mux)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
func (api *Api) MainHandler(w http.ResponseWriter, r *http.Request) {
		ctx :=context.Background()
		output:=make(map[string]interface{})
		output["code"] = 0
		output["success"] = false
		defer func() {
			if err := recover();err != nil{
		   		stack:=debug.Stack()
				fmt.Printf("\r\n %v [Error] [Runtime exception]: <%s> [Stack]: %s ",time.Now(),err,string(stack))
				output["code"] = 54001
				output["msg"] = err
				jsonStr,_:= json.Marshal(output)
				fmt.Fprintf(w,string(jsonStr))
        		r.Body.Close()
			}
		}()
		r.ParseForm()

		path := r.URL.Path
		raw:=getRequestBody(r)
		body:=""
		bodyStr,uerr:= url.QueryUnescape(raw)
		if uerr == nil{
			body = bodyStr
		}else{
			body = raw
		}

		newBody := "";
		if len(bodyStr)>0 {
			newBody=bodyStr[1:len(bodyStr)-1]
		}


		pathArr:=strings.Split(path,"/")

		if pathArr[2]!= "open" && pathArr[2]!= "auth" && pathArr[2]!= "option" {
			panic("without authorization")
		}

		serviceName:=api.Service.ConfMap["services_list"].(map[string]interface{})[pathArr[3]];

		if serviceName == nil{
			panic("Service does not exist")
		}

		method := pathArr[4]
		if api.Service.ConfMap["open"].(map[string]interface{})[pathArr[3]].(map[string]interface{})[method] ==nil && pathArr[2] == "open"{
			panic("Service not accessible")
		}

		catstr :=regexp.MustCompile(`{.*}`).FindAllString(newBody,-1)
		if len(catstr) != 0 && (method=="Acquire" || method == "CreateTask") {
			ret:=catstr[0]
			subUrl := strings.Replace(ret,`"`,`'`,-1);
			body = strings.Replace(bodyStr,ret,subUrl,-1);
		}

		args := new(ProxRequest)
		

		if path != "/" {
			service:=""
			if serviceName != nil {
				service=serviceName.(string)
				args.Service = util.UpFirst(service)
			}
			args.Method = util.UpFirst(method)
		}
		queryStr:=make(map[string]interface{})
		for k,v := range r.Form {
			queryStr[k]=v[0]
		}
		fmt.Println("r.Form===>",r.Form)
		var queryJson map[string]interface{}
		if body != "" {
            jsonErr:=json.Unmarshal([]byte(body),&queryJson)

            if jsonErr == nil{
                for k,val:=range queryJson {
                	queryStr[k] = val
                }
            }else{
            	output["msg"] = jsonErr
            	fmt.Println(jsonErr)
            }
		}
		args.R = queryStr

		args.R["body"] = body
		args.R["header"] = getRequestHeader(r)
		var err error
		reply:=new(Reply)

		if args.Service == "Actuator" {
			output["code"] = 0
			output["msg"] = "ok"
			output["success"] = true
			goto OUT
		} 
		args.R["ip"]=util.RemoteIp(r)
		fmt.Println("args.Service====>",args.Service)
		err = api.Service.ServicePool(args.Service).Call(ctx, args.Method, args, &reply)
		if err != nil {
			panic(fmt.Sprintf("failed to call: %v", err))
		}
		if reply.Result == nil || len(reply.Result) == 0 {
			output["data"] = make(map[string]interface{})
			output["msg"]  = "服务繁忙,请稍后再试"
			output["code"] = 50400
		}else{
			output["code"] = reply.Result["code"]
			output["msg"]  = reply.Result["msg"]
			if reply.Result["data"] != nil {
				output["data"] = reply.Result["data"]
			}else{
				output["data"] = make(map[string]interface{})
			}
			output["success"] = true
		}
		
		OUT:
		jsonStr,_:= json.Marshal(output)
		w.Header().Set("content-type", "application/json;charset=utf-8")
		fmt.Fprint(w,string(jsonStr))
	}

func getRequestRsa(r *http.Request) string {
	r.ParseForm()
	if len(r.Form["rsa"]) > 0 {
		return string(r.Form["rsa"][0]);
	}
	return ""
}
func getRequestHeader(r *http.Request)  map[string][]string {
	return r.Header
}
func getRequestBody(r *http.Request) string {
	s, _ := ioutil.ReadAll(r.Body) //把  body 内容读入字符串 s
	if len(s) > 0 {
		return string(s) //在返回页面中显示内容。
	}else{
		return ""
	}

}
func getRequestCookie(r *http.Request) string{
	cookie, err := r.Cookie("PHPSESSID")
	if err == nil {
		return cookie.Value
	}else{
		return "";
	}
}
