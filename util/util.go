package util
import (
    "encoding/json"
    "strings"
    "fmt"
    io "io/ioutil"
    ioo "io"
    "sync"
    "time"
    "crypto/md5"
    "encoding/hex"
    "math/rand"
    "strconv"
    "net/http"
    "net"
    _"net/url"
    "encoding/base64"

)
var file_locker sync.Mutex
const ossBaseUrl string =""
const ossAvatarUrl string = ""
func JsonToArray(jsonstr string) *[]map[string]interface{} {
    str:=[]byte(jsonstr)
    var resMap []map[string]interface{}
    json.Unmarshal(str,&resMap)
    var tmpMap []map[string]interface{}
    for _,v := range resMap { 
        tmpMap = append(tmpMap,v)
    }
    return &tmpMap   
}
func JsonByteToMap(jsonstr []byte) *map[string]interface{} {
    var resMap map[string]interface{}
    json.Unmarshal(jsonstr,&resMap)
    return &resMap   
}
func JsonByteToArray(jsonstr []byte) *[]interface{} {
    var resMap []interface{}
    json.Unmarshal(jsonstr,&resMap)
    return &resMap   
}
func JsonStringToMap(jsonstr string) *map[string]interface{} {
    str:=[]byte(jsonstr)
    var resMap map[string]interface{}
    json.Unmarshal(str,&resMap)
    return &resMap   
}
func JsonStringToArray(jsonstr string) *[]interface{} {
    str:=[]byte(jsonstr)
    var resMap []interface{}
    json.Unmarshal(str,&resMap)
    return &resMap   
}
func MapToJson(m interface{}) (string,bool) {
    r,e:=json.Marshal(m)
    if e == nil {
        return string(r),true
    }else{
        return "",false
    }
}
func BuildGoodImage(goodsImage string) *string{
    export:=strings.Split(goodsImage,"_")
        if export[0] == "alioss" {
        var imgurlbuile strings.Builder
        imgurlbuile.WriteString(ossBaseUrl)
        imgurlbuile.WriteString(export[1])
        imgurlbuile.WriteString("/")
        imgurlbuile.WriteString(goodsImage)
        goods_image:=imgurlbuile.String()
        return &goods_image
    }else{
        return &goodsImage
    }
}
func BuildMemberAvatar(memberAvatar string) *string{
    export:=strings.Split(memberAvatar,"_")
    if export[0] == "avatar" {
        var imgurlbuile strings.Builder
        imgurlbuile.WriteString(ossAvatarUrl)
        imgurlbuile.WriteString(memberAvatar)
        avatar:=imgurlbuile.String()
        return &avatar
    }else{
        var a string
        a="https://"
        return &a
    }
}
func BuildBase64(str string) string {
    strbytes := []byte(str)
    return base64.StdEncoding.EncodeToString(strbytes)
}
func RandString(len int) string {
    bytes := make([]byte, len)
    for i := 0; i < len; i++ {
        b := rand.Intn(26) + 65
        bytes[i] = byte(b)
    }
    return string(bytes)
}
func BuildUrl(urlArray []string) *string {
    var url strings.Builder
    for _,v := range urlArray {
        url.WriteString(v)
    } 
    res:=url.String()
    return &res
}
func LoadConfig(file string) (map[string]interface{}, bool) {
    var conf map[string]interface{}
    file_locker.Lock()
    data, err := io.ReadFile(file) //read config file
    file_locker.Unlock()
    if err != nil {
        fmt.Println("read json file error")
        return conf, false
    }
    datajson := []byte(data)
    err = json.Unmarshal(datajson, &conf)

    if err != nil {
        fmt.Println("unmarshal json file error")
        return conf, false
    }
    return conf, true
}
func GetTime() int64 {
    // l,_ := time.LoadLocation("Asia/Shanghai")
    createtime := time.Now().Unix()//.In(l).Unix()
    return createtime
}
func GetMd5(str string) string {
    h := md5.New()
    h.Write([]byte(str))
    return hex.EncodeToString(h.Sum(nil))
}
func HttpClient(rtype string,url string) (string,error) {
    if rtype == "" {
        rtype = "GET"
    }
    client:=&http.Client{}
    req,reqerr:=http.NewRequest(rtype,url,nil)
    if reqerr != nil {
        fmt.Println("reqerr",reqerr)
    }
    resp,clienterr:= client.Do(req)
    if clienterr != nil {
        fmt.Println("clienterr",clienterr)
    }
    defer resp.Body.Close()
    body,br:=io.ReadAll(resp.Body)
    return string(body),br
}
func HttpRequest(rtype string,url string,body *string) (string,error) {
    if rtype == "" {
        rtype = "GET"
    }
    fmt.Println("url===>",url)
    var reader ioo.Reader
    if rtype == "POST" {
        reader = strings.NewReader(*body)
    }
    client:=&http.Client{}
    req,reqerr:=http.NewRequest(rtype,url,reader)
    if reqerr != nil {
        fmt.Println("reqerr",reqerr)
    }
    resp,clienterr:= client.Do(req)
    if clienterr != nil {
        fmt.Println("clienterr",clienterr)
    }
    defer resp.Body.Close()
    ret,br:=io.ReadAll(resp.Body)
    fmt.Println("string(body)",string(ret))
    return string(ret),br
}
/*
*@param pwd用户输入的密码
*@param tpwd数据库中的密码
*/
func VirifyUserPwd(pwd string,tpwd string) bool{
    salt := `md5$wg$$%#3ds^^8`
    h:=md5.New()
    h.Write([]byte(pwd))
    ipwd:=hex.EncodeToString(h.Sum(nil))
    var build strings.Builder
    build.WriteString(salt)
    build.WriteString(ipwd)
    h1:=md5.New()
    h1.Write([]byte(build.String()))
    tgpwd:= hex.EncodeToString(h1.Sum(nil))
    if tgpwd == tpwd {
        return true
    }else{
        return false
    }
}
func CreateUserPwd(pwd string) string {
    salt := `md5$wg$$%#3ds^^8`
    h:=md5.New()
    h.Write([]byte(pwd))
    ipwd:=hex.EncodeToString(h.Sum(nil))
    var build strings.Builder
    build.WriteString(salt)
    build.WriteString(ipwd)
    h1:=md5.New()
    h1.Write([]byte(build.String()))
    tgpwd:= hex.EncodeToString(h1.Sum(nil))
    return tgpwd
}
func Round(num int) string {
    rand.Seed(time.Now().Unix())
    var build strings.Builder
    for i:=0;i<num; {
      build.WriteString(strconv.Itoa(rand.Intn(9)))
      i++
    }
    return build.String()
}
func UpFirst(str string) string {
    var upstring string
    val := []rune(str)
    for i := 0; i < len(val); i++ {
        if i == 0 {
            if val[i] >= 97 && val[i] <= 122 { 
                val[i] -= 32 
                upstring += string(val[i])
            } else {
                // fmt.Println("Not begins with lowercase letter,")
                return str
            }
        } else {
            upstring += string(val[i])
        }
    }
    return upstring
}
func RemoteIp(req *http.Request) string {
    remoteAddr := req.RemoteAddr
    if ip := req.Header.Get("Remote_addr"); ip != "" {
        remoteAddr = ip
    } else {
        remoteAddr, _, _ = net.SplitHostPort(remoteAddr)
    }

    if remoteAddr == "::1" {
        remoteAddr = "127.0.0.1"
    }

    return remoteAddr
}
func Out (code int64,msg interface{},data interface{}) map[string]interface{} {
    out:=make(map[string]interface{})
    out["code"] = code
    out["msg"]  = msg
    out["data"] = data
    return out;
}
func CreateRandStr(str string) string {
    now := strconv.FormatInt(GetTime(), 10)
    rand.Seed(time.Now().Unix())
    rdStr := strconv.Itoa(rand.Intn(999999))
    var build strings.Builder
    build.WriteString(str)
    build.WriteString(now)
    build.WriteString(rdStr)
    return GetMd5(build.String())
}
func GetCurrentDate() string {
    now := time.Now()
    year, month, day := now.Date()
    todayStr := fmt.Sprintf("%02d-%02d-%02d", year, month, day)
    return todayStr
}
func GetUTCTime() string{
    now := time.Now()
    year, month, day := now.Date()
    todayStr := fmt.Sprintf("%02d-%02d-%02d %02d:%02d:%02d", year, month, day,now.Hour(),now.Minute(),now.Second())
    return todayStr
}

