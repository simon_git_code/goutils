 package util
    import (
        "crypto"
        "crypto/rand"
        "crypto/rsa"
        "crypto/x509"
        "encoding/base64"
        "encoding/pem"
        "errors"
        "fmt"
        "bytes"

    )
    // 可通过openssl产生
    //openssl genrsa -out rsa_private_key.pem 1024
    //openssl
    //openssl rsa -in rsa_private_key.pem -pubout -out rsa_public_key.pem
    // 加密
    func RsaEncrypt(origData []byte,pubk []byte) ([]byte, error) {
        //解密pem格式的公钥
        block, _ := pem.Decode(pubk)
        if block == nil {
            return nil, errors.New("public key error")
        }
        // 解析公钥
        pubInterface, err := x509.ParsePKIXPublicKey(block.Bytes)
        if err != nil {
            return nil, err
        }
        // 类型断言
        pub := pubInterface.(*rsa.PublicKey)
        //加密
        partLen := pub.N.BitLen() / 8 - 11
        chunks := split([]byte(origData), partLen)
        buffer := bytes.NewBufferString("")
        for _, chunk := range chunks {
            bytes, err := rsa.EncryptPKCS1v15(rand.Reader, pub, chunk)
            if err != nil {
                return make([]byte,0), err
            }
            buffer.Write(bytes)
        }
        return buffer.Bytes(),nil
        // return rsa.EncryptPKCS1v15(rand.Reader, pub, origData)
    }
    // 解密
    func Decrypt(ciphertext []byte,prv []byte) ([]byte, error) {
        //解密
        block, _ := pem.Decode(prv)
        if block == nil {
            return nil, errors.New("private key error!")
        }
        //解析PKCS1格式的私钥
        priv, err := x509.ParsePKCS1PrivateKey(block.Bytes)
        if err != nil {
            return nil, err
        }
        // 解密
        return rsa.DecryptPKCS1v15(rand.Reader, priv, ciphertext)
    }
    func RsaDecrypt(rsastr string,prv []byte) string{
        var destr string
        encode, _:=base64.URLEncoding.DecodeString(rsastr)
        for i:=0;i<len(encode)/128;i++{
            enstr := encode[i*128:128*(i+1)]

            origData, err := Decrypt(enstr,prv)
                    if err !=nil{
                fmt.Println(err)
            }
            destr+=string(origData)
        }
        return destr
    }
    /**
    * 签名
    */
    func Sign(data []byte,algorithmSign crypto.Hash,prv []byte) ([]byte, error) {
        hash := algorithmSign.New()
        hash.Write(data)
        block, _ := pem.Decode(prv)
        if block == nil {
            return nil, errors.New("private key error!")
        }
        //解析PKCS1格式的私钥
        priv, err := x509.ParsePKCS1PrivateKey(block.Bytes)
        sign, err := rsa.SignPKCS1v15(rand.Reader, priv, algorithmSign, hash.Sum(nil))
        if err != nil {
            return nil, err
        }
        return sign, err
    }
    // func main() {
    //     data, _ := RsaEncrypt([]byte("hello world"))
    //     fmt.Println(base64.StdEncoding.EncodeToString(data))
    //     origData, _ := RsaDecrypt(data)
    //     fmt.Println(string(origData))
    // }
func split(buf []byte, lim int) [][]byte {
    var chunk []byte
    chunks := make([][]byte, 0, len(buf)/lim+1)
    for len(buf) >= lim {
        chunk, buf = buf[:lim], buf[lim:]
        chunks = append(chunks, chunk)
    }
    if len(buf) > 0 {
        chunks = append(chunks, buf[:len(buf)])
    }
    return chunks
}