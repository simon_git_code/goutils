package rongcloud
import(
	serversdkgo "github.com/rongcloud/server-sdk-go/v3/sdk"
	// "os"
	"fmt"
)
// rcMsg rcMsg接口
type rcMsg interface {
	ToString() (string, error)
}

type RCloud struct {
	Rc *serversdkgo.RongCloud
}

func CreateRongCloud(key string,sec string) *serversdkgo.RongCloud {
    serversdkgo.NewRongCloud(
        key,
        sec,
    )
    
    rc := serversdkgo.GetRongCloud()
    return rc
}
func (r *RCloud) UserRegister(userId string,userName string) (serversdkgo.User,error ) {
	rep, err := r.Rc.UserRegister(
		userId,
		userName,// "宾购商城",
		"//",
	)
	// fmt.Println("======rc====",rep)
	return rep,err
}
func (r *RCloud) UserStatus(userId string) (int,error){
	rep, err := r.Rc.OnlineStatusCheck(
		userId,
	)
	return rep,err
}

func (r *RCloud) SystemSend (targetID []string, msg string) (error) {
	content := serversdkgo.TXTMsg{
		Content: "hello",
		Extra:   msg,
	}
	fmt.Println(content)
	err := r.Rc.SystemSend(
		"1", 
		targetID, 
		"RC:TxtMsg", 
		&content,
		"文本消息", 
		"", 
		1, 
		0,
	)
	return err
}