package util
import (
	"net"
	"fmt"
)
func GetDockerContainerIp() string {
    netInterfaces, nerr := net.Interfaces()
    if nerr != nil {
        fmt.Println("net.Interfaces failed, err:", nerr.Error())

	}
	addrss:="0.0.0.0"
    for i := 0; i < len(netInterfaces); i++ {
        if (netInterfaces[i].Flags & net.FlagUp) != 0 {
            addrs, _ := netInterfaces[i].Addrs()
 
            for _, address := range addrs {
                if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
                    if ipnet.IP.To4() != nil {
                        fmt.Println(ipnet.IP.String())
                        addrss=ipnet.IP.String()
                        return addrss
                    }
                }
            }
        }
    }
    return addrss
}