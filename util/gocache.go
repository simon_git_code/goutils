package util
import (
    // "fmt"
    redigo "github.com/gomodule/redigo/redis"
    "time"
    "strings"
)
type Redis struct {
	Pool *redigo.Pool
}
//创建连接池
func GetRedisPool(host *string, authkey *string) *redigo.Pool {
    return &redigo.Pool{
        MaxIdle:     10,
        IdleTimeout: 240 * time.Second,
        MaxActive:   300,
        Dial: func() (redigo.Conn, error) {
            c, err := redigo.Dial("tcp", *host)
            if err != nil {
                return nil, err
            }
            if *authkey != "" {
                if _, err := c.Do("AUTH", *authkey); err != nil {
                    c.Close()
                    return nil, err
                }
            }
		    if _, err := c.Do("SELECT", 0); err != nil {
		       c.Close()
		       return nil, err
		    }
            return c, err
        },
        TestOnBorrow: func(c redigo.Conn, t time.Time) error {
            _, err := c.Do("PING")
            return err
        },
    }
}
func (r *Redis) Exec(cmd string,key string,val...string) (interface{}, error) {
	conn:=r.Pool.Get()
    if err := conn.Err(); err != nil {
        return nil, err
    }
    defer conn.Close()
    params := make([]interface{}, 0)
    params = append(params, key)
    if len(val) > 0 {
        for _, v := range val {
            params = append(params, v)
        }
    }
    return conn.Do(cmd, params...)
}
func BuildRedisKey(prefix string,key string) string {
  var build strings.Builder
  build.WriteString(prefix)
  if key != "" {
    build.WriteString(key)
  }
  return build.String()
}
func (r *Redis) CacheGet(key string) (interface {}, error) {
  return r.Exec("get",key)
}
func (r *Redis) CacheSet(key string,val string) (interface {}, error){
  return r.Exec("set",key,val)
}
func (r *Redis) CacheHGet(key string,key1 string) (interface {}, error) {
  return r.Exec("HGET",key,key1)
}
func (r *Redis) CacheHSet(key string,key1 string,val string) (interface {}, error) {
  return r.Exec("HSET",key,key1,val)
}
func (r *Redis) CacheHDel(key string,key1 string) (interface {}, error)  {
  return r.Exec("HDEL",key,key1)
}
func (r *Redis) CacheDel(key string) (interface {}, error)  {
  return r.Exec("DEL",key)
}

