package nacos
import (
		"github.com/nacos-group/nacos-sdk-go/common/constant"
		"github.com/nacos-group/nacos-sdk-go/clients"
		"github.com/nacos-group/nacos-sdk-go/clients/naming_client"
		"github.com/nacos-group/nacos-sdk-go/vo"
		"github.com/smallnest/rpcx/server"
		"github.com/smallnest/rpcx/serverplugin"
		"github.com/smallnest/rpcx/client"
		"log"
		"fmt"
		"runtime"
)
type NacosUtil struct {
	NacosUser *string
	NacosPassword *string
	NacosAddr *string
	NacosPort *int
	NacosDsGroup *string //被发现的微服务组
	GroupName *string //注册组
	Addr *string //注册地址
	Port *uint64
	NamingClient *naming_client.INamingClient
	DataId *string
}
func NewNacosUtil(nacosUser *string,nacosPassword *string,nacosAddr *string,nacosPort *int,nacosDsGroup *string,nacosGroup *string,addr *string,port *uint64,dataid *string) (NacosUtil,error) {
	ci,err:=CreateNacosNamingClient(*nacosAddr,*nacosPort,*nacosUser,*nacosPassword)
	d:=NacosUtil{
		NacosUser:nacosUser,
		NacosPassword:nacosPassword,
		NacosAddr:nacosAddr,
		NacosPort:nacosPort,
		NacosDsGroup:nacosDsGroup,
		GroupName:nacosGroup,
		Addr:addr,
		Port:port,
		NamingClient:&ci,
		DataId:dataid,
	}
	return d,err
}
func CreateNacosNamingClient (NacosAddr string,NacosPort int,NacosUser string,NacosPassword string)(naming_client.INamingClient,error) {
	sysType := runtime.GOOS
	logdir:="/tmp/nacos/log"
	cachedir:="/tmp/nacos/cache"
    if sysType == "windows" {
    	logdir=`.\tmp\nacos\log`
    	cachedir=`.\tmp\nacos\cache`
    }
	sc := []constant.ServerConfig{
		{
			IpAddr: NacosAddr,
			Port:   uint64(NacosPort),
		},
	}
	cc := constant.ClientConfig{
		NamespaceId:         "", //namespace id
		TimeoutMs:           5000,
		NotLoadCacheAtStart: true,
		LogDir:              logdir,
		CacheDir:            cachedir,
		RotateTime:          "1h",
		MaxAge:              3,
		LogLevel:            "debug",
		Username:NacosUser,
		Password:NacosPassword,
	}
	namingClient, err := clients.CreateNamingClient(map[string]interface{}{
	"serverConfigs": sc,
	"clientConfig":  cc,
	})
	if err != nil {
		panic(fmt.Sprintf("clients.CreateNamingClient:error:<%v>",err))
	}
	
	return namingClient,err
}
func (n *NacosUtil) GetNacosSrsList() []string{
	var services []string
	pgSize:=uint32(20)
	param:=vo.GetAllServiceInfoParam{
		PageNo:   1,
		PageSize: pgSize,
		GroupName:*n.NacosDsGroup,
	}

	service, _ := (*n.NamingClient).GetAllServicesInfo(param)
	if service.Doms !=nil{
		resCount:=uint32(service.Count)
		if (resCount/pgSize) > 1{
			var i int64
			for i=2;i<=service.Count;i++ {
				pg:=uint32(i)
				param:=vo.GetAllServiceInfoParam{
					PageNo:   pg,
					PageSize: pgSize,
					GroupName:*n.NacosDsGroup,
				}
				service, _ := (*n.NamingClient).GetAllServicesInfo(param)
				for _,v:=range service.Doms{
					services=append(services,UpFirst(v))
				}
				
			}
		}
		for _,v:=range service.Doms{
			services=append(services,UpFirst(v))
		}	
	}
	return services
}
func(n *NacosUtil) AddRegistryPlugin(s *server.Server) {
	sysType := runtime.GOOS
	logdir:="./log"
	cachedir:="./cache"
    if sysType == "windows" {
    	logdir=`.\log`
    	cachedir=`.\cache`
    }
	clientConfig := constant.ClientConfig{
		TimeoutMs:            10 * 1000,
		ListenInterval:       30 * 1000,
		BeatInterval:         5 * 1000,
		NamespaceId:          "public",
		CacheDir:             cachedir,
		LogDir:               logdir,
		UpdateThreadNum:      20,
		NotLoadCacheAtStart:  true,
		UpdateCacheWhenEmpty: true,
		Username:*n.NacosUser,
		Password:*n.NacosPassword,
	}

	serverConfig := []constant.ServerConfig{{
		IpAddr: *n.NacosAddr,
		Port:   uint64(*n.NacosPort),
	}}

	r := &serverplugin.NacosRegisterPlugin{
		ServiceAddress: "tcp@" + *n.Addr,
		ClientConfig:   clientConfig,
		ServerConfig:   serverConfig,
		Cluster:        "test",
		GroupName:		*n.NacosDsGroup,
	}
	err := r.Start()
	if err != nil {
		log.Fatal(err)
	}
	s.Plugins.Add(r)
}
func(n *NacosUtil) ConfigNacos(v string) (client.ServiceDiscovery ,error){
	sysType := runtime.GOOS
	logdir:="./log"
	cachedir:="./cache"
    if sysType == "windows" {
    	logdir=`.\log`
    	cachedir=`.\cache`
    }
	clientConfig := constant.ClientConfig{
		TimeoutMs:            10 * 1000,
		ListenInterval:       30 * 1000,
		BeatInterval:         5 * 1000,
		NamespaceId:          "public",
		CacheDir:             cachedir,
		LogDir:               logdir,
		UpdateThreadNum:      20,
		NotLoadCacheAtStart:  true,
		UpdateCacheWhenEmpty: true,
		Username:*n.NacosUser,
		Password:*n.NacosPassword,
	}

	serverConfig := []constant.ServerConfig{{
		IpAddr: *n.NacosAddr,
		Port:   uint64(*n.NacosPort),
	}}

	return client.NewNacosDiscovery(v, "", clientConfig, serverConfig,*n.NacosDsGroup)

}

func(n *NacosUtil) RegisterService(servicename string,group *string) error{

 	addrss := *n.Addr
 	addrss = GetDockerContainerIp()
	_, err := (*n.NamingClient).RegisterInstance(vo.RegisterInstanceParam{
	    Ip:          addrss,
	    Port:        *n.Port,
	    ServiceName: servicename,
	    Weight:      10,
	    Enable:      true,
	    Healthy:     true,
	    Ephemeral:   true,
	    // Metadata:    map[string]string{"idc":"shanghai"},
	    // ClusterName: "cluster-a", // 默认值DEFAULT
	    GroupName:   *group,  // 默认值DEFAULT_GROUP
	})
	return err
}

func (n *NacosUtil) GetConfig(config *map[string]interface{}) {
	sysType := runtime.GOOS
	logdir:="/tmp/nacos/log"
	cachedir:="/tmp/nacos/cache"
    if sysType == "windows" {
    	logdir=`.\tmp\nacos\log`
    	cachedir=`.\tmp\nacos\cache`
    }
	clientConfig := constant.ClientConfig{
		NamespaceId:         "", // 如果需要支持多namespace，我们可以场景多个client,它们有不同的NamespaceId
		TimeoutMs:           5000,
		NotLoadCacheAtStart: true,
		LogDir:              logdir,
		CacheDir:            cachedir,
		RotateTime:          "1h",
		MaxAge:              3,
		LogLevel:            "debug",
		Username:*n.NacosUser,
		Password:*n.NacosPassword,
	} 
	serverConfigs := []constant.ServerConfig{
	    {
	        IpAddr:      *n.NacosAddr,
	        ContextPath: "/nacos",
	        Port:        uint64(*n.NacosPort),
	        Scheme:      "http",
	    },
	}
	configClient, _ := clients.NewConfigClient(
	    vo.NacosClientParam{
	        ClientConfig:  &clientConfig,
	        ServerConfigs: serverConfigs,
	    },
	)
	cerr := configClient.ListenConfig(vo.ConfigParam{
	    DataId: *n.DataId,
	    Group:  *n.GroupName,
	    OnChange: func(namespace, group, dataId, data string) {
			*config = *JsonStringToMap(data)
			fmt.Println("配置变更了=====",*config)
		},
	})
	//获取配置
	content, _ := configClient.GetConfig(vo.ConfigParam{
    DataId: *n.DataId,
    Group:  *n.GroupName})
    *config = *JsonStringToMap(content)
    // fmt.Println("========",config)

	fmt.Println(cerr)
}
