package es
import(
	"fmt"
	"github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esapi"
	"context"
	"encoding/json"
	"runtime/debug"
	"log"
	"bytes"
)
type ES struct {
	Client *elasticsearch.Client
	Index string
	Config map[string]interface{}
}

func (es *ES) Builder(index... interface{})  {
    defer func() {
        if err := recover();err != nil{
	   		stack:=debug.Stack()
			fmt.Printf("\r\n %v [Error] [Runtime exception]: <%s> [Stack]: %s ",GetUTCTime(),err,string(stack))
        }
    }()
	config:=es.Config
	cfg := elasticsearch.Config{
		Addresses: []string{
		config["host"].(string),
		},
		Username: config["username"].(string),
		Password: config["password"].(string),
	}
	if index!= nil {
		es.Index = index[0].(string)
	}else{
		es.Index = config["index"].(string)
	}
	
	c, err := elasticsearch.NewClient(cfg)
	if err != nil {
		log.Printf("Error creating the client: %s", err)
		panic(fmt.Sprintf("Error creating the client:%v",err))
	} else {
		es.Client = c
	}
}
func (es *ES) Update(id string,body map[string]interface{}) {
    defer func() {
        if err := recover();err != nil{
	   		stack:=debug.Stack()
			fmt.Printf("\r\n %v [Error] [Runtime exception]: <%s> [Stack]: %s ",GetUTCTime(),err,string(stack))
        }
    }()
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(body); err != nil {
		log.Printf("Error encoding query: %s", err)
		panic(fmt.Sprintf("Error encoding query: %v",err))
	}
	c:=es.Client
	res, err := c.Update(
		es.Index,
		id,
		&buf,
		c.Update.WithPretty(),
	)
	if err != nil {
		log.Printf("Error getting response: %s", err)
		panic(fmt.Sprintf("Error getting response:%v",err))
	}
	// fmt.Println(res)
	defer res.Body.Close()
}
func (es *ES) CatIndices() *esapi.Response {
	// var e map[string]interface{}
	c:=es.Client
	res, err := c.Cat.Indices(
	 	c.Cat.Indices.WithContext(context.Background()),
 		c.Cat.Indices.WithIndex(es.Index),
 		c.Cat.Indices.WithH("docs.deleted"),
		c.Cat.Indices.WithPretty(),
	)
	if err != nil {
		log.Printf("Error getting response: %s", err)
	}
	// defer res.Body.Close()

	return res
}
func (es *ES) Search(query map[string]interface{}) (interface{},float64) {
    defer func() {
        if err := recover();err != nil{
	   		stack:=debug.Stack()
			fmt.Printf("\r\n %v [Error] [Runtime exception]: <%s> [Stack]: %s ",GetUTCTime(),err,string(stack))
        }
    }()
	var buf bytes.Buffer
	var ret []interface{}
	var tot float64
    // fmt.Println(query);
	if err := json.NewEncoder(&buf).Encode(query); err != nil {
		log.Printf("Error encoding query: %s", err)
		panic(fmt.Sprintf("Error encoding query:%v",err))
	}
	c:=es.Client
	res, err := c.Search(
		c.Search.WithContext(context.Background()),
		c.Search.WithIndex(es.Index),
		c.Search.WithBody(&buf),
		c.Search.WithTrackTotalHits(true),
		c.Search.WithPretty(),
	)
	if err != nil {
		log.Printf("Error getting response: %s", err)
		panic(fmt.Sprintf("Error getting response:%v",err))
	}
	if res == nil{
		return ret,tot
	}
	defer res.Body.Close()

	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
		  log.Printf("Error parsing the response body: %s", err)
		} else {
		  // Print the response status and error information.
		  log.Printf("[%s] %s: %s",
		    res.Status(),
		    e["error"].(map[string]interface{})["type"],
		    e["error"].(map[string]interface{})["reason"],
		  )
		}
	}
	var r map[string]interface{}
	decode:= json.NewDecoder(res.Body)
	if err:=decode.Decode(&r); err != nil {
		log.Printf("Error parsing the response body: %s", err)
		panic(fmt.Sprintf("Error parsing the response body:%v",err))
	}
	// Print the response status, number of results, and request duration.
	// log.Printf(
	// "[%s] %d hits; took: %dms",
	// res.Status(),
	// int(r["hits"].(map[string]interface{})["total"].(map[string]interface{})["value"].(float64)),
	// int(r["took"].(float64)),
	// )

	// Print the ID and document source for each hit.

	if r != nil {
		tot = r["hits"].(map[string]interface{})["total"].(map[string]interface{})["value"].(float64)
		for _, hit := range r["hits"].(map[string]interface{})["hits"].([]interface{}) {
			var retMap interface{}
			if hit != nil{
				retMap=hit.(map[string]interface{})["_source"]
			}
			retMap.(map[string]interface{})["_id"] = hit.(map[string]interface{})["_id"]
			ret = append(ret,retMap)
		}
	}else{
		tot = 0
	}
	return ret,tot
}
func (es *ES) SearchV2(query map[string]interface{}) (interface{},float64) {
    defer func() {
        if err := recover();err != nil{
	   		stack:=debug.Stack()
			fmt.Printf("\r\n %v [Error] [Runtime exception]: <%s> [Stack]: %s ",GetUTCTime(),err,string(stack))
        }
    }()
	var buf bytes.Buffer
	var ret []interface{}
	var tot float64
    // fmt.Println(query);
	if err := json.NewEncoder(&buf).Encode(query); err != nil {
		log.Printf("Error encoding query: %s", err)
		panic(fmt.Sprintf("Error encoding query:%v",err))
	}
	c:=es.Client
	res, err := c.Search(
		c.Search.WithContext(context.Background()),
		c.Search.WithIndex(es.Index),
		c.Search.WithBody(&buf),
		c.Search.WithTrackTotalHits(true),
		c.Search.WithPretty(),
	)
	if err != nil {
		log.Printf("Error getting response: %s", err)
		panic(fmt.Sprintf("Error getting response:%v",err))
	}
	if res == nil{
		return ret,tot
	}
	defer res.Body.Close()

	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
		  log.Printf("Error parsing the response body: %s", err)
		} else {
		  // Print the response status and error information.
		  log.Printf("[%s] %s: %s",
		    res.Status(),
		    e["error"].(map[string]interface{})["type"],
		    e["error"].(map[string]interface{})["reason"],
		  )
		}
	}
	var r map[string]interface{}
	decode:= json.NewDecoder(res.Body)
	decode.UseNumber()
	if err:=decode.Decode(&r); err != nil {
		log.Printf("Error parsing the response body: %s", err)
		panic(fmt.Sprintf("Error parsing the response body:%v",err))
	}
	// Print the response status, number of results, and request duration.
	// log.Printf(
	// "[%s] %d hits; took: %dms",
	// res.Status(),
	// int(r["hits"].(map[string]interface{})["total"].(map[string]interface{})["value"].(float64)),
	// int(r["took"].(float64)),
	// )

	// Print the ID and document source for each hit.

	if r != nil {
		tot ,_= r["hits"].(map[string]interface{})["total"].(map[string]interface{})["value"].(json.Number).Float64()
		for _, hit := range r["hits"].(map[string]interface{})["hits"].([]interface{}) {
			var retMap interface{}
			if hit != nil{
				retMap=hit.(map[string]interface{})["_source"]
			}
			retMap.(map[string]interface{})["_id"] = hit.(map[string]interface{})["_id"]
			ret = append(ret,retMap)
		}
	}else{
		tot = 0
	}
	return ret,tot
}