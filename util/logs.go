package util
import (
	"log"
	"io"
	"os"
	// "fmt"
	// "bytes"
	// "syscall"
	"runtime/debug"
)
var stdErrFileHandler *os.File
func WriteLogs (logs interface{},file... string) {
	path:=""
		if file == nil {
			path="/var/log/golang/goservices_auth_err.log"
		}else{
			path=file[0]
		}
		// writer1 := &bytes.Buffer{}
		// writer2 := os.Stdout
		writer, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)
		defer writer.Close()
		if err != nil {
		    log.Println("create file log.txt failed: %v", err)
		}
		  // logger := log.New(io.MultiWriter(writer1, writer2, writer3), "", log.Lshortfile|log.LstdFlags)
		logger := log.New(io.MultiWriter(writer), "", log.Llongfile|log.LstdFlags)
		logger.Println(logs)
		logger.Println(string(debug.Stack()))
}