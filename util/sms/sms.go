package sms
import(
	"fmt"
  	"github.com/aliyun/alibaba-cloud-sdk-go/services/dysmsapi"
  	"strings"
)
func SendLoginSms(mobile string,code string) bool {
	client, err := dysmsapi.NewClientWithAccessKey("", "", "")

	request := dysmsapi.CreateSendSmsRequest()
	request.Scheme = "https"
  	request.PhoneNumbers = mobile
  	request.SignName = ""
  	request.TemplateCode = ""
  	var build strings.Builder
  	build.WriteString("{\"code\":")
  	build.WriteString(code)
  	build.WriteString("}")
  	request.TemplateParam = build.String()
	response, err := client.SendSms(request)
	if err != nil {
		fmt.Print(err.Error())
		return false
	}else{
		return true
	}
	fmt.Printf("response is %#v\n", response)
	return false
	
}