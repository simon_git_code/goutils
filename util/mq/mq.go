
package mq

// import (
// 	"fmt"
// 	"github.com/gogap/errors"
// 	"strings"
// 	"time"
// 	"github.com/aliyunmq/mq-http-go-sdk"
// 	"encoding/json"
// )
// type Mq struct {
// 	Endpoint string
// 	// AccessKey 阿里云身份验证，在阿里云服务器管理控制台创建
// 	AccessKey string
// 	// SecretKey 阿里云身份验证，在阿里云服务器管理控制台创建
// 	SecretKey string
// 	// 所属的 Topic
// 	Topic string
// 	// Topic所属实例ID，默认实例为空
// 	InstanceId string
// 	// 您在控制台创建的 Consumer ID(Group ID)
// 	GroupId string
// 	Client mq_http_sdk.MQClient
// }

// func NewMqClient(conf map[string]interface{},mqname string) Mq {

// 	topid:=conf["topic"].(map[string]interface{})
	
// 	d:=Mq{
// 		Endpoint:conf["endpoint"].(string),
// 		AccessKey:conf["accessKey"].(string),
// 		SecretKey:conf["secretKey"].(string),
// 		Topic:topid[mqname].(string),
// 		InstanceId:conf["instanceId"].(string),
// 		GroupId:conf["groupId"].(string),
// 	}
// 	d.Client = mq_http_sdk.NewAliyunMQClient(d.Endpoint, d.AccessKey, d.SecretKey, "")
// 	return d
// }
// func(mq *Mq) SendMsg(msgStr string) {
// 	mqProducer := mq.Client.GetProducer(mq.InstanceId, mq.Topic)
// 	var msg mq_http_sdk.PublishMessageRequest
//     msg = mq_http_sdk.PublishMessageRequest{
//         MessageBody: msgStr,         //消息内容
//         MessageTag:  "",                  // 消息标签
//         Properties:  map[string]string{}, // 消息属性
//     }
//     // 设置KEY
//     msg.MessageKey = CreateRandStr("")
//     ret, err := mqProducer.PublishMessage(msg)

//     if err != nil {
//         fmt.Println(err)
//         return
//     } else {
//         fmt.Printf("Publish ---->\n\tMessageId:%s, BodyMD5:%s, \n", ret.MessageId, ret.MessageBodyMD5)
//     }
// }
// func(mq *Mq) ConsumerMsg(mqFun func(map[string]interface{})) {
// 	mqConsumer :=  mq.Client.GetConsumer(mq.InstanceId, mq.Topic, mq.GroupId, "")
// 	for {
// 		endChan := make(chan int)
// 		respChan := make(chan mq_http_sdk.ConsumeMessageResponse)
// 		errChan := make(chan error)
// 		go func() {
// 			select {
// 			case resp := <-respChan:
// 				{
// 					// 处理业务逻辑
// 					var handles []string
// 					fmt.Printf("Consume %d messages---->\n", len(resp.Messages))
// 					for _, v := range resp.Messages {
// 						handles = append(handles, v.ReceiptHandle)
// 						fmt.Printf("\tMessageID: %s, PublishTime: %d, MessageTag: %s\n"+
// 							"\tConsumedTimes: %d, FirstConsumeTime: %d, NextConsumeTime: %d\n"+
//                             "\tBody: %s\n"+
//                             "\tProps: %s\n",
// 							v.MessageId, v.PublishTime, v.MessageTag, v.ConsumedTimes,
// 							v.FirstConsumeTime, v.NextConsumeTime, v.MessageBody, v.Properties)
// 							var jsMap map[string]interface{}
// 							jsErr:=json.Unmarshal([]byte(v.MessageBody),&jsMap)
// 							if jsErr == nil {
// 								fmt.Println("执行写入===",jsMap)
// 								mqFun(jsMap)
// 							}
// 					}
//                     // NextConsumeTime前若不确认消息消费成功，则消息会重复消费
//                     // 消息句柄有时间戳，同一条消息每次消费拿到的都不一样
//                     ackerr := mqConsumer.AckMessage(handles)
// 					if ackerr != nil {
// 						// 某些消息的句柄可能超时了会导致确认不成功
// 						fmt.Println(ackerr)
// 						for _, errAckItem := range ackerr.(errors.ErrCode).Context()["Detail"].([]mq_http_sdk.ErrAckItem) {
// 							fmt.Printf("\tErrorHandle:%s, ErrorCode:%s, ErrorMsg:%s\n",
// 								errAckItem.ErrorHandle, errAckItem.ErrorCode, errAckItem.ErrorMsg)
// 						}
// 						time.Sleep(time.Duration(3) * time.Second)
// 					} else {
// 						fmt.Printf("Ack ---->\n\t%s\n", handles)
// 					}
// 					endChan <- 1
// 				}
// 			case err := <-errChan:
// 				{
// 					// 没有消息
// 					if strings.Contains(err.(errors.ErrCode).Error(), "MessageNotExist") {
// 						// fmt.Println("\nNo new message, continue!")
// 					} else {
// 						fmt.Println(err)
// 						time.Sleep(time.Duration(3) * time.Second)
// 					}
// 					endChan <- 1
// 				}
// 			case <-time.After(35 * time.Second):
// 				{
// 					fmt.Println("Timeout of consumer message ??")
// 					endChan <- 1
// 				}
// 			}
// 		}()

// 		// 长轮询消费消息
// 		// 长轮询表示如果topic没有消息则请求会在服务端挂住3s，3s内如果有消息可以消费则立即返回
// 		mqConsumer.ConsumeMessage(respChan, errChan,
// 			3, // 一次最多消费3条(最多可设置为16条)
// 			3, // 长轮询时间3秒（最多可设置为30秒）
// 		)
// 		<-endChan
// 	}
// }
