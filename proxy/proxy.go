package proxy

import(
    // "crypto/tls"
    "net/http/cookiejar"
    "gitee.com/simon_git_code/godb/db"
    "math/rand"
    "net/http"
    // "net"
    "net/url"
    "time"
    "io/ioutil"
    "fmt"
)

func CreateProxy(conn *mysql.Db) *http.Client {
    ipdb:=conn.Db()
    ipdb.Field("data,speed,type2,type1")
    ipdb.Table("_ip")
    ipdb.Where("speed<?")
    rows:=ipdb.Select("1000")
    rdm := rand.Intn(len(rows)-1)
    data := rows[rdm]
    var testIP string
    if data["type2"] == "https" {
        testIP = "https://" + data["data"].(string)
    } else {
        testIP = "http://" + data["data"].(string)
    }
    proxy, _ := url.Parse(testIP)
fmt.Println("testIP===>",proxy);
    // tlsConfig := &tls.Config{InsecureSkipVerify: true}

    // netTransport := &http.Transport{
    //     Proxy:               http.ProxyURL(proxy),
    //     // TLSClientConfig:     tlsConfig,
    //     MaxIdleConnsPerHost: 50,
    // }
    jar, _ := cookiejar.New(nil)
    httpClient := &http.Client{
        Timeout:   time.Second * 20,
        // Transport: netTransport,
        Jar:jar,
    }
    return httpClient
}
func HandleRequest(uri string,client *http.Client,header map[string]string ) *[]byte {
    req,err := http.NewRequest("GET", uri, nil)
    if err != nil {
    	fmt.Println(err);
    }
    if header != nil {
    	for i,v:=range header{
    		req.Header.Add(i,v)
    	}
    }
    agent:=[]string{
        "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Mobile Safari/537.36",
        "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Mobile Safari/537.36",
    }
    rdm := rand.Intn(len(agent)-1)
    fmt.Println("agent[rdm]====>",agent[rdm])
    
    req.Header.Add("User-Agent",agent[0])
    resp,error := client.Do(req)
    if error != nil {
        fmt.Println("error",error)
    	panic("请求第三方服务器错误")
    }

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
	    // handle error
	}
	defer resp.Body.Close()
	return &body
}
func CreateBaseClient() *http.Client {
    jar, _ := cookiejar.New(nil)
    httpClient := &http.Client{
        Timeout:   time.Second * 20,
        Jar:jar,
    }
    return httpClient
}
func HandleBaseRequest(uri string,header map[string]string,rtype string ) *[]byte {
    if rtype=="" {
        rtype="GET"
    }
    req,err := http.NewRequest(rtype, uri, nil)
    if err != nil {
        fmt.Println(err);
    }
    if header != nil {
        for i,v:=range header{
            req.Header.Add(i,v)
        }
    }
    client:=CreateBaseClient()
    resp,error := client.Do(req)
    if error != nil {
        fmt.Println("error",error)
        panic("请求第三方服务器错误")
    }

    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        // handle error
    }
    defer resp.Body.Close()
    return &body
}