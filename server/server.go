package server
import(
	"flag"
	"gitee.com/simon_git_code/goutils/util"
	"github.com/smallnest/rpcx/client"
	"fmt"
)
var (
	nacosAddr   = flag.String("nacosAddr", "192.168.1.1", "nacos address")
	nacosPort   = flag.Int("nacosPort", 8848, "nacos bind port") 
	addr   = flag.String("addr", "0.0.0.0", "container address")
	nacosUser = flag.String("nacosUser", "nacos", "nacos user name")
	nacosPassword = flag.String("nacosPassword", "nacos", "nacos password")
	nacosGroup = flag.String("nacosGroup", "GO_API_GROUP", "nacos group")
	nacosDsGroup = flag.String("nacosDsGroup", "GO_SERVICE_GROUP", "nacos descover group")
	nacosDataId = flag.String("nacosDataId", "spider-dev.json", "nacos group")
)
type Service struct{
	ConfMap map[string]interface{}
	NacosUtil util.NacosUtil 
	Services map[string] client.XClient
}
func Init() Service {
	flag.Parse()
	port := uint64(9092)
	addrss :=""
	if (*addr != "0.0.0.0") {
		addrss = *addr
	} else {
		addrss=util.GetDockerContainerIp()
	}
	s := Service{}
	nacosUtil,err:=util.NewNacosUtil(nacosUser,nacosPassword,nacosAddr,nacosPort,nacosDsGroup,nacosGroup,&addrss,&port,nacosDataId)
	if err !=nil {
		panic("注册中心连接失败")
	}else{
		s.NacosUtil = nacosUtil
	}

	confMap:=make(map[string]interface{})
	s.NacosUtil.GetConfig(&confMap)
	s.ConfMap = confMap
	fmt.Println("服务配置",confMap)
	rerr:=nacosUtil.RegisterService("go-service",nacosGroup)
	if rerr != nil {
		panic("注册微服务失败")
	}
	service :=nacosUtil.GetNacosSrsList()
	services:=make(map[string] client.XClient)
	for _,v := range service {
		d,_:= nacosUtil.ConfigNacos(v)
		v = util.UpFirst(v)
		c := client.NewXClient(v, client.Failtry, client.RandomSelect, d, client.DefaultOption)
		services[v] = c
	}
	s.Services = services
	return s
}
func (s *Service) ServicePool(serviceName string) client.XClient {
	if (s.Services[serviceName] == nil) {
		d,_:= s.NacosUtil.ConfigNacos(serviceName)
		xclient := client.NewXClient(serviceName, client.Failtry, client.RandomSelect, d, client.DefaultOption)
		s.Services[serviceName] = xclient
	}
	return s.Services[serviceName]
}
